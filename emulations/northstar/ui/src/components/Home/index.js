import React, { Component } from 'react'
import PropTypes from 'prop-types'

import agent from 'agent'
import { connect } from 'react-redux'
import {
  LOAD_TOPOLOGY_DATA,
  LOAD_DEVICES_DATA,
  SET_EMULATION,
  LOAD_EMULATIONS_DATA,
  APP_LOAD
} from 'constants/actionTypes'

import Graph from './Graph'
import EmulationSelector from './EmulationSelector'
import Loading from './Loading'

const mapStateToProps = state => ({
  ...state.topologies,
  appLoaded: state.common.appLoaded
})

const mapDispatchToProps = dispatch => ({
  loadTopologies: payload => dispatch({ type: LOAD_TOPOLOGY_DATA, payload }),
  loadDevices: payload => dispatch({ type: LOAD_DEVICES_DATA, payload }),
  loadEmulations: payload => dispatch({ type: LOAD_EMULATIONS_DATA, payload }),
  setEmulation: payload => dispatch({ type: SET_EMULATION, payload }),
  onLoad: () => dispatch({ type: APP_LOAD })
})

class Home extends Component {
  async componentDidMount() {
    await this._loadGraph()
  }

  render() {
    let { topologies, devices, appLoaded } = this.props
    let graph
    if (topologies && topologies !== {} && devices) {
      let nodes = this._createNodes(devices)
      let edges = this._createEdges(topologies, nodes)

      // creating graph with computed nodes and edges
      graph = { nodes, edges }
    }
    return (
      <div>
        <EmulationSelector
          emulations={this.props.emulations}
          selected_emultaion={this.props.selected_emulation}
          onEmulationClick={async emulation => {
            this.props.setEmulation(emulation)
            await this._loadGraph()
          }}
        />
        {appLoaded ? <Graph graph={graph} /> : <Loading />}
      </div>
    )
  }

  _createNodes = devices => {
    return devices.slice(0, -1).map(n => {
      return {
        id: parseInt(n.id, 10),
        label: n.name
      }
    })
  }

  _createEdges = (topologies, nodes) => {
    let edges = topologies.map(edge => {
      let from = nodes.find(node => node.label === edge.device)
      let to = nodes.find(node => node.label === edge.neighbor)
      if (from && to) {
        return {
          from: from.id,
          to: to.id
        }
      } else {
        return null
      }
    })

    // removing null edges from array
    return edges.filter(e => e)
  }

  _loadGraph = async () => {
    let {
      loadEmulations,
      loadTopologies,
      loadDevices,
      setEmulation,
      selected_emulation,
      onLoad
    } = this.props

    // load all user emulations
    loadEmulations((await agent.Topology.getEmulations()).data)

    if (!selected_emulation) {
      // set current emulation to the first in the array
      setEmulation({ name: this.props.emulations[0].name })
    }

    // load All devices in current emulation
    loadDevices(await agent.Topology.getDevices(this.props.selected_emulation))

    // load topology for current emulation
    loadTopologies(
      (await agent.Topology.getTopologies(this.props.selected_emulation)).data
    )
    onLoad()
  }
}

Home.propTypes = {
  loadTopologies: PropTypes.func,
  loadDevices: PropTypes.func,
  setEmulation: PropTypes.func,
  loadEmulations: PropTypes.func,
  topologies: PropTypes.array,
  devices: PropTypes.array
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
