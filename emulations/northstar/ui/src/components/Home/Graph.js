import React, { Component } from 'react'
import PropTypes from 'prop-types'
import NetworkGraph from 'react-graph-vis'
import { graph as styles } from 'constants/styles'

const options = {
  layout: {
    hierarchical: false
  },
  edges: {
    color: '#fff'
  }
}

const events = {
  select: function(event) {
    var { nodes, edges } = event
  }
}

class Graph extends Component {
  render() {
    return (
      <div style={styles.wrapper}>
        {this.props.graph && (
          <NetworkGraph
            graph={this.props.graph}
            options={options}
            events={events}
          />
        )}
      </div>
    )
  }
}

Graph.propTypes = {}

export default Graph
