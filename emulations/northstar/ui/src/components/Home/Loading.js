import React, { Component } from 'react'

import { GridLoader } from 'react-spinners'

import { common as styles } from 'constants/styles'
import colors from 'constants/colors'

class Login extends Component {
  render() {
    return (
      <div
        style={{
          ...styles.flex_col_center_justified,
          height: '100vh',
          backgroundColor: colors.blue_dark
        }}
      >
        <GridLoader size={50} color={colors.white} />
      </div>
    )
  }
}

Login.propTypes = {}

export default Login
