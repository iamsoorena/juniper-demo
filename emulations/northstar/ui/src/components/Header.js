import React, { Component } from 'react'

import Logo from './Logo'

import { header as styles } from 'constants/styles'

class Header extends Component {
  render() {
    return (
      <div style={styles.header_wrapper}>
        <Logo />
        <div>
          <h1 style={styles.header}>Emulations</h1>
        </div>
      </div>
    )
  }
}

Header.propTypes = {}

export default Header
