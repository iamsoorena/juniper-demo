import React, { Component } from 'react'

import Logo from './Logo'

import { common as styles } from 'constants/styles'
import colors from 'constants/colors'

class Login extends Component {
  render() {
    return (
      <div
        style={{
          ...styles.flex_col_center_justified,
          height: '100vh',
          backgroundColor: colors.blue_dark
        }}
      >
        <Logo size={'200px'} />
        <input
          placeholder={'Token'}
          type="text"
          // todo: refactor input styles.
          style={{
            display: 'block',
            marginTop: '40px',
            background: 'none',
            padding: '0.175rem 0.175rem 0.0875rem',
            fontSize: '1.1rem',
            borderWidth: 0,
            borderColor: 'transparent',
            borderBottom: '2px',
            borderBottomColor: 'white',
            borderBottomStyle: 'solid',
            lineHeight: '1.9',
            width: '400px',
            color: '#ccc',
            transition: 'all 0.28s ease',
            boxShadow: 'none'
          }}
          onKeyPress={this._handleKeyPress}
        />
      </div>
    )
  }

  _handleKeyPress = event => {
    if (event.key === 'Enter') {
      this.props.registerToken(event.target.value)
    }
  }
}

Login.propTypes = {}

export default Login
