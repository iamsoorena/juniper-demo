import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import topologies from './reducers/topologies'
import common from './reducers/commmon'

export default combineReducers({
  common,
  topologies,
  router: routerReducer
})
