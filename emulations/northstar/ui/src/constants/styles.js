import colors from 'constants/colors'
import values from 'constants/values'

export const common = {
  flex_row_center: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  flex_col_center: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  flex_row_center_justified: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  flex_col_center_justified: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  }
}

export const graph = {
  wrapper: {
    height: `calc(100vh - ${values.HEADER_HEIGHT})`,
    backgroundColor: colors.gray
  }
}

export const logo = {
  marginLeft: '15px',
  width: '100px',
  marginRight: '20px',
  transform: 'translateY(5px)'
}

export const header = {
  header_wrapper: {
    backgroundColor: colors.blue_dark,
    height: values.HEADER_HEIGHT,
    width: '100%',
    margin: 0,
    ...common.flex_row_center
  },
  header: {
    color: colors.white,
    fontSize: values.H1
  }
}
