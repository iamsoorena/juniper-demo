# TESUTO Test project

all project development files are kept in `src` folder.

## commands

run project in development mode on port `4100`, you can change this later in `package.json`
    
    yarn
    yarn start
    
build project for production:

    yarn
    yarn build

## run project

server built project:
    
    yarn
    yarn build
    cd build
    python -m SimpleHTTPServer 3000